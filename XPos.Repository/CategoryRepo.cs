﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xpos.Model;
using XPos.ViewModel;

namespace XPos.Repository
{
    public class CategoryRepo
    {
       //Get All
       public static List<CategoryViewModel> GetAll()
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();

            using (var db = new XPosContext())
            {
                result = (from cat in db.Categories
                          select new CategoryViewModel
                          {
                              Id = cat.Id,
                              Initial = cat.Initial,
                              Name = cat.Name,
                              Active = cat.Active
                          }).ToList();
            }
            return result;
        }

        //Get By Id
        public static CategoryViewModel GetbyId(int id)
        {
            CategoryViewModel result = new CategoryViewModel();

            using (var db = new XPosContext())
            {
                result = (from cat in db.Categories
                          where cat.Id == id
                          select new CategoryViewModel
                          {
                              Id = cat.Id,
                              Initial = cat.Initial,
                              Name = cat.Name,
                              Active = cat.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new CategoryViewModel();
        }

        // Update
        public static ResponseResult Update (CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region 
                    if (entity.Id == 0)
                    {
                        Category category = new Category();

                        category.Initial = entity.Initial;
                        category.Name = entity.Name;
                        category.Active = entity.Active;

                        category.CreatedBy = "Admin";
                        category.CreatedDate = DateTime.Now;

                        db.Categories.Add(category);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    #endregion
                    #region 
                    else
                    {
                        Category category = db.Categories.Where(o => o.Id == entity.Id).FirstOrDefault();

                        if (category != null)
                        {
                        
                            category.Initial = entity.Initial;
                            category.Name = entity.Name;
                            category.Active = entity.Active;

                            category.CreatedBy = "Admin";
                            category.CreatedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Category is not found";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception)
            {
                result.Success = false;
                result.Message = "Category is not found";
                
            }
            return result;
        }

        // Delete
        public static ResponseResult Delete (CategoryViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Category category = db.Categories.Where(o => o.Id == entity.Id).FirstOrDefault();

                    if (category != null)
                    {
                        db.Categories.Remove(category);
                        db.SaveChanges();
                      
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Category is not found";
                    }
                }
            }
            catch (Exception)
            {
                result.Success = false;
                result.Message = "Category is not found";
            }
            return result;
        }

        //Get By Search
        public static List<CategoryViewModel> GetBySearch(string search)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .Where(o => o.Active == true && (o.Name.Contains(search)))
                    .Take(10)
                    .Select(p => new CategoryViewModel
                    {
                        Id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        //Order By Ascending
        public static List<CategoryViewModel> GetByAscending(string Name)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .OrderBy(o => o.Name)
                    .Select(p => new CategoryViewModel
                    {
                        Id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        //Order By Descending
        public static List<CategoryViewModel> GetByDescending(string Name)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .OrderByDescending(o => o.Name)
                    .Select(p => new CategoryViewModel
                    {
                        Id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        // Active List
        public static List<CategoryViewModel> GetActiveList(string Name)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .Where(o => o.Active == true)
                    .Take(10)
                    .Select(p => new CategoryViewModel
                    {
                        Id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        // Non Active List
        public static List<CategoryViewModel> GetNonactiveList(string Name)
        {
            List<CategoryViewModel> result = new List<CategoryViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Categories
                    .Where(o => o.Active == false)
                    .Take(10)
                    .Select(p => new CategoryViewModel
                    {
                        Id = p.Id,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }
    }
}
