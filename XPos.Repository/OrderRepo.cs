﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xpos.Model;
using XPos.ViewModel;

namespace XPos.Repository
{
    public class OrderRepo
    {
        public static ResultOrder Post
            (OrderHeaderViewModel entity)
        {
            ResultOrder result = new ResultOrder();
            try
            {
                using (var db = new XPosContext())
                {
                    string newReference = NewReference();

                    OrderHeader orderHeader = new OrderHeader();

                    orderHeader.Reference = newReference;
                    orderHeader.Amount = entity.Amount;
                    orderHeader.Active = true;

                    orderHeader.CreatedBy = "Admin";
                    orderHeader.CreatedDate = DateTime.Now;

                    db.OrderHeaders.Add(orderHeader);

                    foreach (var item in entity.Details)
                    {
                        OrderDetail orderDetail = new OrderDetail();
                        Product products = new Product();
                        orderDetail.HeaderId = orderHeader.Id;
                        orderDetail.ProductId = item.ProductId;
                        products.Stocks = item.Stocks;
                        orderDetail.Price = item.Price;
                        orderDetail.Quantity = item.Quantity;
                        orderDetail.Active = true;

                        orderDetail.CreatedBy = "Admin 2";
                        orderDetail.CreatedDate = DateTime.Now;

                        Product product = db.Products.Where(o => o.Id == orderDetail.ProductId).FirstOrDefault();
                        product.Stocks -= orderDetail.Quantity;

                        if(product.Stocks < orderDetail.Quantity)
                        {
                            result.Success = false;
                            result.Message = "Your quantity of the order is greater than the Stocks!";
                            break;
                        }
                        db.OrderDetails.Add(orderDetail);

                        db.SaveChanges();
                        result.Reference = newReference;                       
                    }             
                }
            }
            catch(Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
        }

        public static String NewReference()
        {
            string yearMonth = DateTime.Now.ToString("yy") +
                DateTime.Now.Month.ToString("D2");

            string result = "SLS-" + yearMonth + "-";

            using (var db = new XPosContext())
            {
                var maxReference = db.OrderHeaders
                    .Where(oh => oh.Reference.Contains(result))
                    .Select(oh => new { reference = oh.Reference })
                    .OrderByDescending(oh => oh.reference)
                    .FirstOrDefault();

                if(maxReference != null)
                {
                    string[] oldReference = maxReference.reference.Split('-');
                    int newIncrement = int.Parse(oldReference[2]) + 1;
                    result += newIncrement.ToString("D4");
                }
                else
                {
                    result += "0001";
                }
            }
            return result;

        }
    }
}
