﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xpos.Model;
using XPos.ViewModel;

namespace XPos.Repository
{
    public class ProductRepo
    {
        //Get All
        //public static List<ProductViewModel> GetAll()
        //{
        //List<ProductViewModel> result = new List<ProductViewModel>();

        //using (var db = new XPosContext())
        // {
        //result = (from pro in db.Products
        //select new ProductViewModel
        //{
        // Id = pro.Id,
        // Initial = pro.Initial,
        // Name = pro.Name,
        // Active = pro.Active
        // }).ToList();
        //}
        //return ByVarian(-1);
        //}

        // Get All
        public static Tuple<List<ProductViewModel>, int> GetAll(int page, int count)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            int rowNumber = 0;

            using (var db = new XPosContext())
            {
                var query = db.Products;

                rowNumber = query.Count();
                result = query
                    .OrderBy(p => p.Id)
                    .Skip((page - 1) * count)
                    .Take(count)
                    .Select(c => new ProductViewModel
                    {
                        Id = c.Id,
                        VariantId = c.VariantId,
                        VariantName = c.Variant.Name,
                        CategoryId = c.Variant.CategoryId,
                        CategoryName = c.Variant.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Description = c.Description,
                        Price = c.Price,
                        Active = c.Active,
                        Stocks = c.Stocks

                    })
                    .ToList();
                return new Tuple<List<ProductViewModel>, int>(result, rowNumber);
            }
        }

        //Get By Filter
        public static Tuple<List<ProductViewModel>, int> GetByFilter (string search)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            int count = 0;

            using (var db = new XPosContext())
            {
                var query = db.Products
                    .Where(p => p.Initial.Contains(search) ||
                    p.Name.Contains(search) || p.Description.Contains
                    (search));
                count = query.Count();
                result = query
                    //.Take(5)
                    .Select(c => new ProductViewModel
                    {
                        Id = c.Id,
                        VariantId = c.VariantId,
                        VariantName = c.Variant.Name,
                        CategoryId = c.Variant.CategoryId,
                        CategoryName = c.Variant.Category.Name,
                        Initial = c.Initial,
                        Name = c.Name,
                        Description = c.Description,
                        Price = c.Price,
                        Active = c.Active,
                        Stocks = c.Stocks
                    })
                    .ToList();
            }
            return new Tuple<List<ProductViewModel>, int>(result, count);
        }

        //By Varian
        public static List<ProductViewModel> ByVarian(long id)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                .Where(v => v.VariantId == (id == -1 ? v.VariantId : id))
                .Select(c => new ProductViewModel
                {
                    Id = c.Id,
                    VariantId = c.VariantId,
                    VariantName = c.Variant.Name,
                    Initial = c.Initial,
                    Name = c.Name,
                    Description = c.Description,
                    Price = c.Price,
                    Active = c.Active,
                    Stocks = c.Stocks
                }).ToList();
            }
            return result;
        }

        //Get By Id
        public static ProductViewModel GetbyId(int id)
        {
            ProductViewModel result = new ProductViewModel();

            using (var db = new XPosContext())
            {
                result = (from pro in db.Products
                          where pro.Id == id
                          select new ProductViewModel
                          {
                              Id = pro.Id,
                              Initial = pro.Initial,
                              CategoryId = pro.Variant.CategoryId,
                              VariantId = pro.VariantId,
                              Name = pro.Name,
                              Description = pro.Description,
                              Price = pro.Price,
                              Active = pro.Active,
                              Stocks = pro.Stocks
                          }).FirstOrDefault();
            }
            return result != null ? result : new ProductViewModel();
        }

        // Update
        public static ResponseResult Update(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region 
                    if (entity.Id == 0)
                    {
                        Product product = new Product();

                        product.VariantId = entity.VariantId;
                        product.Initial = entity.Initial;
                        product.Name = entity.Name;
                        product.Description = entity.Description;
                        product.Price = entity.Price;
                        product.Active = entity.Active;
                        product.Stocks = entity.Stocks;
                        product.CreatedBy = "Admin";
                        product.CreatedDate = DateTime.Now;

                        if (product.Stocks < 0 || product.Price < 0)
                        {
                            result.Success = false;
                            result.Message = "Your number is must not be less than zero!";
                        }
                        else
                        {
                            db.Products.Add(product);
                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        
                    }
                    #endregion
                    #region 
                    else
                    {
                        Product product = db.Products.Where(o => o.Id == entity.Id).FirstOrDefault();

                        if (product != null)
                        {
                            product.VariantId = entity.VariantId;
                            product.Initial = entity.Initial;
                            product.Name = entity.Name;
                            product.Description = entity.Description;
                            product.Price = entity.Price;
                            product.Active = entity.Active;
                            product.Stocks = entity.Stocks;
                            product.CreatedBy = "Admin";
                            product.CreatedDate = DateTime.Now;

                            if (product.Stocks < 0 && product.Price < 0)
                            {
                                result.Success = false;
                                result.Message = "Your number is must not be less than zero!";
                            }
                            else
                            {
                                db.SaveChanges();

                                result.Entity = entity;
                            }                            
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Product is not found";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception)
            {
                result.Success = false;
                result.Message = "Product is not found";

            }
            return result;
        }

        // Delete
        public static ResponseResult Delete(ProductViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Product product = db.Products.Where(o => o.Id == entity.Id).FirstOrDefault();

                    if (product != null)
                    {
                        db.Products.Remove(product);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Product is not found";
                    }
                }
            }
            catch (Exception)
            {
                result.Success = false;
                result.Message = "Product is not found";
            }
            return result;
        }

        //Get By Search
        public static List<ProductViewModel> GetBySearch(string search)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .Where(o => o.Active == true && ( o.Name.Contains(search) ))
                    .Take(10)
                    .Select(p => new ProductViewModel
                    {
                        Id = p.Id,
                        VariantId = p.VariantId,
                        VariantName = p.Variant.Name,
                        //CategoryName = p.Variant.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        Active = p.Active,
                        Stocks = p.Stocks
                    }).ToList();
            }
            return result;
        }

        // Order By Ascending
        public static List<ProductViewModel> GetByAscending(string Name)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .OrderBy(o => o.Name)
                    .Select(p => new ProductViewModel
                    {
                        Id = p.Id,
                        VariantId = p.VariantId,
                        VariantName = p.Variant.Name,
                        //CategoryName = p.Variant.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        Active = p.Active,
                        Stocks = p.Stocks
                    }).ToList();
            }
            return result;
        }

        // Order By Descending
        public static List<ProductViewModel> GetByDescending(string Name)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .OrderByDescending(o => o.Name)
                    .Select(p => new ProductViewModel
                    {
                        Id = p.Id,
                        VariantId = p.VariantId,
                        VariantName = p.Variant.Name,
                        //CategoryName = p.Variant.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        Active = p.Active,
                        Stocks = p.Stocks
                    }).ToList();
            }
            return result;
        }

        // Active List
        public static List<ProductViewModel> GetActiveList(string Name)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .Where(o => o.Active == true)
                    .Take(10)
                    .Select(p => new ProductViewModel
                    {
                        Id = p.Id,
                        VariantId = p.VariantId,
                        VariantName = p.Variant.Name,
                        //CategoryName = p.Variant.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        Active = p.Active,
                        Stocks = p.Stocks
                    }).ToList();
            }
            return result;
        }

        // Non Active List
        public static List<ProductViewModel> GetNonactiveList(string Name)
        {
            List<ProductViewModel> result = new List<ProductViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Products
                    .Where(o => o.Active == false)
                    .Take(10)
                    .Select(p => new ProductViewModel
                    {
                        Id = p.Id,
                        VariantId = p.VariantId,
                        VariantName = p.Variant.Name,
                        //CategoryName = p.Variant.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Description = p.Description,
                        Price = p.Price,
                        Active = p.Active,
                        Stocks = p.Stocks
                    }).ToList();
            }
            return result;
        }
    }
}
