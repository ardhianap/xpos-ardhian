﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xpos.Model;
using XPos.ViewModel;

namespace XPos.Repository
{
    public class VariantRepo
    {
        //Get All
        public static List<VariantViewModel> GetAll()
        {
        //List<VariantViewModel> result = new List<VariantViewModel>();

        // using (var db = new XPosContext())
        // {
        //  result = (from vari in db.Variants
        // select new VariantViewModel
        // {
        //   Id = vari.Id,
        //    Initial = vari.Initial,
        //    Name = vari.Name,
        //    Active = vari.Active
        // }).ToList();
        //}
        return ByCategory(-1);
        }

        // Get All
      //  public static Tuple<List<VariantViewModel>, int> GetAll(int page, int count)
      //  {
         //   List<VariantViewModel> result = new List<VariantViewModel>();
         //   int rowNumber = 0;

         //   using (var db = new XPosContext())
         //   {
             //   var query = db.Variants;

              //  rowNumber = query.Count();
            //    result = query
                //    .OrderBy(p => p.Id)
                //    .Skip((page - 1) * count)
                 //   .Take(count)
                //    .Select(c => new VariantViewModel
                 //  {
                  //      Id = c.Id,
                  //      CategoryId = c.CategoryId,
                   //     CategoryName = c.Category.Name,
                   //     Initial = c.Initial,
                    //    Name = c.Name,
                   //     Active = c.Active
                   // })
                  //  .ToList();
                //return new Tuple<List<VariantViewModel>, int>(result, rowNumber);
            //}
        //}

        //By Category
        public static List<VariantViewModel> ByCategory (long id)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                .Where(v => v.CategoryId == (id == -1 ? v.CategoryId : id))
                .Select(c => new VariantViewModel
                {
                    Id = c.Id,
                    CategoryId = c.CategoryId,
                    CategoryName = c.Category.Name,
                    Initial = c.Initial,
                    Name = c.Name,
                    Active = c.Active
                }).ToList();
            }
            return result;
        }

        //Get By Id
        public static VariantViewModel GetbyId(int id)
        {
            VariantViewModel result = new VariantViewModel();

            using (var db = new XPosContext())
            {
                result = (from vari in db.Variants
                          where vari.Id == id
                          select new VariantViewModel
                          {
                              Id = vari.Id,
                              Initial = vari.Initial,
                              Name = vari.Name,
                              Active = vari.Active
                          }).FirstOrDefault();
            }
            return result != null ? result : new VariantViewModel();
        }

        // Update
        public static ResponseResult Update(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    #region 
                    if (entity.Id == 0)
                    {
                        Variant variant = new Variant();

                        variant.CategoryId = entity.CategoryId;
                        variant.Initial = entity.Initial;
                        variant.Name = entity.Name;
                        variant.Active = entity.Active;
                        variant.CreatedBy = "Admin";
                        variant.CreatedDate = DateTime.Now;

                        db.Variants.Add(variant);
                        db.SaveChanges();

                        result.Entity = entity;
                    }
                    #endregion
                    #region 
                    else
                    {
                        Variant variant = db.Variants.Where(o => o.Id == entity.Id).FirstOrDefault();

                        if (variant != null)
                        {
                            variant.CategoryId = entity.CategoryId;
                            variant.Initial = entity.Initial;
                            variant.Name = entity.Name;
                            variant.Active = entity.Active;
                            variant.CreatedBy = "Admin";
                            variant.CreatedDate = DateTime.Now;

                            db.SaveChanges();

                            result.Entity = entity;
                        }
                        else
                        {
                            result.Success = false;
                            result.Message = "Variant is not found";
                        }
                        #endregion
                    }
                }
            }
            catch (Exception)
            {
                result.Success = false;
                result.Message = "Variant is not found";

            }
            return result;
        }

        // Delete
        public static ResponseResult Delete(VariantViewModel entity)
        {
            ResponseResult result = new ResponseResult();
            try
            {
                using (var db = new XPosContext())
                {
                    Variant variant = db.Variants.Where(o => o.Id == entity.Id).FirstOrDefault();

                    if (variant != null)
                    {
                        db.Variants.Remove(variant);
                        db.SaveChanges();
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "Variant is not found";
                    }
                }
            }
            catch (Exception)
            {
                result.Success = false;
                result.Message = "Variant is not found";
            }
            return result;
        }

        //Get By Search
        public static List<VariantViewModel> GetBySearch(string search)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .Where(o => o.Active == true && (o.Name.Contains(search)))
                    .Take(10)
                    .Select(p => new VariantViewModel
                    {
                        Id = p.Id,
                        CategoryId = p.CategoryId,
                        CategoryName = p.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        //Order By Ascending
        public static List<VariantViewModel> GetByAscending(string Name)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .OrderBy(o => o.Name)
                    .Select(p => new VariantViewModel
                    {
                        Id = p.Id,
                        CategoryId = p.CategoryId,
                        CategoryName = p.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        //Order By Descending
        public static List<VariantViewModel> GetByDescending(string Name)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .OrderByDescending(o => o.Name)
                    .Select(p => new VariantViewModel
                    {
                        Id = p.Id,
                        CategoryId = p.CategoryId,
                        CategoryName = p.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        // Active List
        public static List<VariantViewModel> GetActiveList(string Name)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .Where(o => o.Active == true)
                    .Take(10)
                    .Select(p => new VariantViewModel
                    {
                        Id = p.Id,
                        CategoryId = p.CategoryId,
                        CategoryName = p.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }

        // Non Active List
        public static List<VariantViewModel> GetNonactiveList(string Name)
        {
            List<VariantViewModel> result = new List<VariantViewModel>();
            using (var db = new XPosContext())
            {
                result = db.Variants
                    .Where(o => o.Active == false)
                    .Take(10)
                    .Select(p => new VariantViewModel
                    {
                        Id = p.Id,
                        CategoryId = p.CategoryId,
                        CategoryName = p.Category.Name,
                        Initial = p.Initial,
                        Name = p.Name,
                        Active = p.Active
                    }).ToList();
            }
            return result;
        }
    }
}
