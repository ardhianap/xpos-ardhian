﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.Repository;
using XPos.ViewModel;

namespace XPoss.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List(int page = 1, int count = 10)
        {
            var ListProduct = ProductRepo.GetAll(page, count);
            decimal pageTotal = (decimal)ListProduct.Item2 / count;
            int fullPage = ListProduct.Item2 / count;

            if (pageTotal - fullPage > 0)
            {
                fullPage += 1;
            }
            ViewBag.PageTotal = fullPage;
            return PartialView("_List", ListProduct.Item1);
        }

        //public ActionResult List()
        //{
        //List<ProductViewModel> ListVariant = ProductRepo.GetAll();
        //return PartialView("_List", ListVariant);
        //}

        public ActionResult ActiveList(string Name)
        {
            return PartialView("_List", ProductRepo.GetActiveList(Name));
        }

        public ActionResult NonactiveList(string Name)
        {
            return PartialView("_List", ProductRepo.GetNonactiveList(Name));
        }

        public ActionResult OrderAscList(string Name)
        {
            return PartialView("_List", ProductRepo.GetByAscending(Name));
        }

        public ActionResult OrderDescList(string Name)
        {
            return PartialView("_List", ProductRepo.GetByDescending(Name));
        }

        public ActionResult ProductList(string search ="")
        {
            return PartialView("_List", ProductRepo.GetBySearch(search));
        }

        public ActionResult ListByVarian(long Id = 0)
        {
            return PartialView("_ListByVarian", ProductRepo.ByVarian(Id));
        }

        public ActionResult ListByCategory(long Id = 0)
        {
            return PartialView("_ListByCategory", VariantRepo.ByCategory(Id));
        }

        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "Id", "Name");
            ViewBag.VarianList = new SelectList(VariantRepo.GetAll(), "Id", "Name");

            //ViewBag.VarianList = new SelectList(VariantRepo.ByCategory(0), "Id", "Name");
            return PartialView("_Create", new ProductViewModel());
        }

        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        //edit
        public ActionResult Edit(int id)
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "Id", "Name");
            ViewBag.VarianList = new SelectList(VariantRepo.GetAll(), "Id", "Name");
            return PartialView("_Edit", ProductRepo.GetbyId(id));
        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }

        //delete
        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", ProductRepo.GetbyId(id));
        }

        [HttpPost]
        public ActionResult Delete(ProductViewModel model)
        {
            ResponseResult result = ProductRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ProductListForOrder(string search = "")
        {
            var result = ProductRepo.GetByFilter(search);
            return PartialView("_ProductListForOrder", result.Item1);
        }
    }
}