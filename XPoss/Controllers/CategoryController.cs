﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.Repository;
using XPos.ViewModel;

namespace XPoss.Controllers
{
    public class CategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            return PartialView("_List", CategoryRepo.GetAll());
        }

        public ActionResult OrderAscList(string Name)
        {
            return PartialView("_List", CategoryRepo.GetByAscending(Name));
        }

        public ActionResult OrderDescList(string Name)
        {
            return PartialView("_List", CategoryRepo.GetByDescending(Name));
        }

        public ActionResult ActiveList(string Name)
        {
            return PartialView("_List", CategoryRepo.GetActiveList(Name));
        }

        public ActionResult NonactiveList(string Name)
        {
            return PartialView("_List", CategoryRepo.GetNonactiveList(Name));
        }

        public ActionResult CategoryList(string search = "")
        {
            return PartialView("_List", CategoryRepo.GetBySearch(search));
        }

        //create
        public ActionResult Create()
        {
            return PartialView("_Create", new CategoryViewModel());
        }

        [HttpPost]
        public ActionResult Create (CategoryViewModel model)
        {
            ResponseResult result = CategoryRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
         
        }

        //edit
        public ActionResult Edit(int id)
        {
            return PartialView("_Edit", CategoryRepo.GetbyId(id));
        }

        [HttpPost]
        public ActionResult Edit(CategoryViewModel model)
        {
            ResponseResult result = CategoryRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }

        //delete
        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", CategoryRepo.GetbyId(id));
        }

        [HttpPost]
        public ActionResult Delete(CategoryViewModel model)
        {
            ResponseResult result = CategoryRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }
    }
}