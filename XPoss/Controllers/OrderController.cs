﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.Repository;
using XPos.ViewModel;

namespace XPoss.Controllers
{
    public class OrderController : Controller
    {
        // GET: Order
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SelectedProduct(int id)
        {
            ProductViewModel product = ProductRepo.GetbyId(id);
            OrderDetailViewModel detail = new OrderDetailViewModel();
            detail.ProductId = product.Id;
            detail.ProductName = product.Name;
            detail.Price = product.Price;
            detail.Stocks = product.Stocks;
            return PartialView("_SelectedProduct", detail);

        }

        [HttpPost]
        public ActionResult Payment(OrderHeaderViewModel model)
        {
            return PartialView("_Payment", model);
        }

        [HttpPost]
        public ActionResult Pay (OrderHeaderViewModel model)
        {
            ResultOrder result = OrderRepo.Post(model);
            return Json(
                new
                {
                    success = result.Success,
                    message = result.Message,
                    reference = result.Reference
                }, JsonRequestBehavior.AllowGet);
        }
    }
}