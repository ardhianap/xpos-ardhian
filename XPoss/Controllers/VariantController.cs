﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using XPos.Repository;
using XPos.ViewModel;

namespace XPoss.Controllers
{
    public class VariantController : Controller
    {
        // GET: Variant
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {
            List<VariantViewModel> ListVariant = VariantRepo.GetAll();
        return PartialView("_List", ListVariant);
        }

        //public ActionResult List(int page = 1, int count = 10)
        //{
           // var ListProduct = VariantRepo.GetAll(page, count);
           // decimal pageTotal = (decimal)ListProduct.Item2 / count;
           // int fullPage = ListProduct.Item2 / count;

           // if (pageTotal - fullPage > 0)
           // {
          //      fullPage += 1;
           // }
           // ViewBag.PageTotal = fullPage;
           // return PartialView("_List", ListProduct.Item1);
       // }

        public ActionResult ActiveList(string Name)
        {
            return PartialView("_List", VariantRepo.GetActiveList(Name));
        }

        public ActionResult NonactiveList(string Name)
        {
            return PartialView("_List", VariantRepo.GetNonactiveList(Name));
        }

        public ActionResult OrderAscList(string Name)
        {
            return PartialView("_List", VariantRepo.GetByAscending(Name));
        }

        public ActionResult OrderDescList(string Name)
        {
            return PartialView("_List", VariantRepo.GetByDescending(Name));
        }

        public ActionResult VariantList(string search = "")
        {
            return PartialView("_List", VariantRepo.GetBySearch(search));
        }

        public ActionResult ListByCategory(long Id = 0)
        {
            return PartialView("_ListByCategory", VariantRepo.ByCategory(Id));
        }

        //create
        public ActionResult Create()
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "Id", "Name");
            return PartialView("_Create", new VariantViewModel());
        }

        [HttpPost]
        public ActionResult Create(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        //edit
        public ActionResult Edit(int id)
        {
            ViewBag.CategoryList = new SelectList(CategoryRepo.GetAll(), "Id", "Name");
            return PartialView("_Edit", VariantRepo.GetbyId(id));
        }

        [HttpPost]
        public ActionResult Edit(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }

        //delete
        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", VariantRepo.GetbyId(id));
        }

        [HttpPost]
        public ActionResult Delete(VariantViewModel model)
        {
            ResponseResult result = VariantRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);

        }
    }
}